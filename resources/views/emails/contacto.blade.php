<h1>Mensaje desde el formulario de contacto</h1>
<p>Se recibió el siguiente mensaje desde el formulario de contacto:</p>
<p><strong>Nombre del remitente:</strong> {{ $request->name }}</p>
<p><strong>Correo electrónico:</strong> {{ $request->email }}</p>
@if ($request->phone)
	<p><strong>Teléfono:</strong> {{ $request->phone }}</p>
@endif
<p><strong>Mensaje:</strong><br> {{ $request->message }}</p>
<br>
<p><small><a href='http://jairo.rocks'>JAIRO.ROCKS</a></small></p>