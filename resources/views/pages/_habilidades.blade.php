<div class="bb-item" id="page-4">
    <div class="bb-custom-side">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Habilidades</span></h1>
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" data-transitiongoal="70">
                            <span class="sr-only">70% Complete (success)</span>
                        </div>
                        <span class="progress-type">PHP</span>
                        <span class="progress-completed">70%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" data-transitiongoal="75"></div>
                        <span class="progress-type">Laravel</span>
                        <span class="progress-completed">75%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" data-transitiongoal="60"></div>
                        <span class="progress-type">HTML5</span>
                        <span class="progress-completed">60%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" data-transitiongoal="50"></div>
                        <span class="progress-type">CSS3</span>
                        <span class="progress-completed">50%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" data-transitiongoal="45"></div>
                        <span class="progress-type">Javascript</span>
                        <span class="progress-completed">45%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" data-transitiongoal="45"></div>
                        <span class="progress-type">jQuery</span>
                        <span class="progress-completed">50%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" data-transitiongoal="30"></div>
                        <span class="progress-type">Vue.js</span>
                        <span class="progress-completed">30%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" data-transitiongoal="70"></div>
                        <span class="progress-type">MySql</span>
                        <span class="progress-completed">70%</span>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-offset-2 animateFadeInUp">
                    <div class="well">
                        <h3>Desarrollo Web</h3>
                        <p>
                            Como desarrollador web he procurado aprender sobre tecnologías novedosas pero a la vez potentes y sacar lo mejor de cada una. A la hora de desarrollar trato de, según las necesidades del proyecto, integrar estas tecnologías de manera que se dé solución a problemas y se cumpla con los objetivos. Si bien no soy un gurú en HTML5 / CSS3, tengo "debilidad" por los entornos gráficos minimalistas y a la vez con buen UI / UX que se pueden lograr con estas tecnologías.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 animateFadeInUp">
                    <div class="well">
                        <h3>Backend</h3>
                        <p>
                            Como desarrollador principalmente enfocado al Backend, trato de estar aprendiendo constantemente nuevas cosas. Tengo habilidad principalmente para PHP, pero he encontrado muy interesante y efectivo combinarlo con tecnologías como Node.js, Socket.io y Redis. Dentro del mismo PHP, el uso del lenguaje y de buenas prácticas de programación es algo en lo que procuro siempre estar actualizado.
                        </p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>