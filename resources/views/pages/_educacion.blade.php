<div class="bb-item" id="page-2">
    <div class="bb-custom-side">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Educación</span></h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="blockquote-box blockquote-primary clearfix animateBounceInLeft">
                        <div class="square pull-left">
                            <i class="fa fa-calendar"></i> 1998 - 2004
                        </div>
                        <h2>Mariano Suárez Veintimilla</h2>
                        <p class="text-justify">
                            Mis estudios secundarios los realicé en el Colegio Nacional Técnico <strong>Mariano Suárez Veitimilla</strong> . El 30 de julio de 2004 me gradué el título de Bachiller en Comercio y Administración Especialización Informática.
                        </p>
                    </div>
                    <div class="blockquote-box blockquote-danger clearfix animateBounceInLeft">
                        <div class="square pull-left">
                            <i class="fa fa-calendar"></i> 2004 - 2008
                        </div>
                        <h2>ITSI</h2>
                        <p class="text-justify">
                            Empezando en 2004, estudié Sistemas en el <strong>Instituto Tecnológico Superior Ibarra</strong>, y el 17 de marzo de 2008 obtuve el título de Tecnólogo en Sistemas.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="blockquote-box blockquote-warning clearfix animateBounceInRight">
                        <div class="square pull-left">
                            <i class="fa fa-calendar"></i> 2009 - 2012
                        </div>
                        <h2>UNIANDES</h2>
                        <p class="text-justify">
                            En 2012 obtuve el título de Ingeniero en Sistemas e Informática. Los estudios para este título los realicé en  en la <strong><a href="http://www.uniandes.edu.ec/" target="_blank" rel="nofollow">Universidad Regional Autónoma de los Andes</a></strong> (UNIANDES).
                        </p>
                    </div>
                    <div class="blockquote-box blockquote-success clearfix animateBounceInRight">
                        <div class="square pull-left">
                            <i class="fa fa-calendar"></i> 2014
                        </div>
                        <h2>Mejorando.la</h2>
                        <p class="text-justify">
                            En agosto del 2014 aprobé el Curso Profesional de PHP con Laravel en <a href="https://mejorando.la/" target="_blank"><strong>Mejorando.la</strong></a>. Certificación de aprobación online: <a href="https://cursos.mejorando.la/usuarios/jago86" target="_blank">Click aquí</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>