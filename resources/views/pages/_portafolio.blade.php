<div class="bb-item" id="page-5">
    <div class="bb-custom-side" id="portfolio">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Trabajo</span></h1>
            <div class="row">

                <div class="col-sm-6 col-md-4 text-center animateFlipInX">
                    <div class="portfolio-item">
                        <p><img src="images/gallito-pucara.png" alt="Guiatecuador.com" style="height: 128px;"></p>
                        <h4><a href="http://guiatecuador.com" target="_blank">Grupopucara.com</a></h4>
                        <p>Sitio web para el Grupo Cultral Indígena Pucará. Grupo ecuatoriano que cuenta con mas de 40 años de trayectoria nacional e internacional.</p>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 text-center animateFlipInX">
                    <div class="portfolio-item">
                        <p><img src="images/guiatecuador.png" alt="Guiatecuador.com" width="128"></p>
                        <h4><a href="http://guiatecuador.com" target="_blank">Guiatecuador.com</a></h4>
                        <p>Guía de comercial para Ecuador. Desarrollé este proyecto usando Laravel y actualmente me encargo de administrarlo y mantenerlo.</p>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 text-center animateFlipInX">
                    <div class="portfolio-item">
                        <p><img src="images/briefcase.png" alt="Porfinempleo.com"/ alt="maletín"></p>
                        <h4><a href="http://www.porfinempleo.com" target="_blank">Porfinempleo.com</a></h4>
                        <p>Desarrollo de varias secciones y módulos: sistema de administración para clientes y candidatos. Notificaciones en tiempo real con Node.js, etc.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 text-center animateFlipInY">
                    <div class="portfolio-item">
                        <p><img src="images/cupon.png" alt="cupón"/></p>
                        <h4><a href="http://www.cuponagil.com" target="_blank">Cuponagil.com</a></h4>
                        <p>Portal web para descarga de cupones de descuento en Ecuador (ya no disponible actualmente).</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 text-center animateFlipInX">
                    <div class="portfolio-item">
                        <p><img src="images/horse.png" alt="caballo"/></p>
                        <h4><a href="http://www.accae.net/" target="_blank">ACCAE</a></h4>
                        <p>Construcción módulos para presentar los ejemplares y el árbol genealógico de los ejemplares registrados en la ACCAE. <a href="http://www.accae.net/accae/?page_id=1335" target="_blank">Lista completa</a>, <a href="http://www.accae.net/accae/?page_id=1331" target="_blank">padrillos</a>, <a href="http://www.accae.net/accae/?page_id=1329" target="_blank">yeguas</a>, <a href="http://www.accae.net/accae/?page_id=1327" target="_blank" >potros y potras</a></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 text-center animateFlipInY">
                    <div class="portfolio-item">
                        <p><img src="images/mangueras.png" alt="magueras"/></p>
                        <h4><a href="http://chiribogayjara.com/site/" target="_blank" >Chiriboga & Jara</a></h4>
                        <p>Portal web desarrollado con la finalidad de dar a conocer información de la empresa y el catálogo de productos. Desarrollado usando PHP, HTML, CSS, MYSQL.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 text-center">
                    <div class="portfolio-item animateFlipInY">
                        <p><img src="images/grupo.png" alt="grupo personas"/></p>
                        <h4><a href="http://www.cedet.ec/" target="_blank">CEDET</a></h4>
                        <p>Portal web para el Comite Ecuatoriano de Desarrollo Económico y Territorial. Uno de mis primeros trabajos como freelance (sitio no disponible actualmente) </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>