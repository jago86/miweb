<div class="bb-item" id="page-6">
    <div class="bb-custom-side">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Contáctame</span></h1>
            <div class="row">
                <div class="col-sm-6">
                    <form id="contact-form" class="form-horizontal" onsubmit="sendContactEmail(); return false;">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Nombre</label>
                                <div class="col-md-9">
                                    <input id="name" name="name" type="text" placeholder="Tu nombre" class="form-control" required="required">
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Correo electrónico</label>
                                <div class="col-md-9">
                                    <input id="email" name="email" type="text" placeholder="Tu correo electrónico" class="form-control" required="required">
                                </div>
                            </div>
                            <!-- Phone input -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Teléfono (opcional)</label>
                                <div class="col-md-9">
                                    <input id="phone" name="phone" type="text" placeholder="Tu número de teléfono" class="form-control">
                                </div>
                            </div>
                            <!-- Message body -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="message">Mensaje</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="message" name="message" placeholder="Escribe tu mensaje aquí..." rows="5" required="required"></textarea>
                                </div>
                            </div>
                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <input id="send-contact" type="submit" class="btn btn-success btn-lg" value="Enviar">
                                </div>
                            </div>
                            <div id="ajax-state-contact">
                                <div class="bubblingG">
                                    <span id="bubblingG_1">
                                    </span>
                                    <span id="bubblingG_2">
                                    </span>
                                    <span id="bubblingG_3">
                                    </span>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div id="contact-map"></div>
                    <address class="pull-right">
                        <strong>Durán, Guayas, Ecuador</strong><br>
                        <abbr title="Phone">Telf:</abbr> (593) 993523071
                    </address>
                </div>
            </div>
        </div>
    </div>
</div>