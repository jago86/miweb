<div class="bb-item" id="page-3">
    <div class="bb-custom-side" id="experience">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Experiencia</span></h1>
            <div class="row">
                <div class="col-sm-6 animateFadeInDown">
                    <div class="panel panel-default">
                        <div class="panel-heading"><img src="images/logo_pfe.png" height="40" width="40" alt="logo de Porfinempleo.com"> Porfinempleo</div>
                        <div class="panel-body">
                            <a href="http://www.porfinempleo.com" target="_blank" >Porfinempleo</a> es un portal que se dedica a publicar anuncios de trabajo en línea de empresas de todo el país (Ecuador).
                        </div>
                        <div class="panel-footer">Analista IT, marzo 2014 hasta noviembre 2014</div>
                    </div>
                </div>
                <div class="col-sm-6 animateFadeInUp">
                    <div class="panel panel-default">
                        <div class="panel-heading"><img src="images/cuponagil-logo.png" height="40" width="40" alt="Logo de cuponagil"> Cuponagil</div>
                        <div class="panel-body">
                            <a href="http://www.cuponagil.com" target="_blank">Cuponagil</a> fue un portal dedicado a ofrecer cupones de descuento para negocios y tiendas de distinta índole,principalmete en la ciudad de Quito.
                        </div>
                        <div class="panel-footer">Trabajé como Programador Web Senior, desde 2012 hasta 2013</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 animateFadeInUp">
                    <div class="panel panel-default">
                        <div class="panel-heading"><img src="images/qualyhost.jpg" height="40" width="40" alt="Logo de Qualyhost"> Qualyhost</div>
                        <div class="panel-body">
                            <a href="http://www.qualyhost.com/" target="_blank">Qualyhost</a>, empresa proveedora de servicios de hosting y desarrollo web.
                        </div>
                        <div class="panel-footer">Pasantías en el 2007. Departamento de Desarrollo y Programación Web desde 2007 hasta 2009; desde 2008 como jefe de este departamento.</div>
                    </div>
                </div>
                <div class="col-sm-6 animateFadeInDown">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-2x fa fa-paper-plane-o black"></i> Freelance</div>
                        <div class="panel-body">
                            Permanentemente hago trabajos en modalidad Freelance como: sitios web sencillos, portales, módulos para sitios ya desarrollados, asesoría, etc.
                        </div>
                        <div class="panel-footer">Freelance en varios proyectos, desde 2009</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 animateFadeInDown">
                    <div class="panel panel-default">
                       <div class="panel-heading"><img src="images/nova.jpg" height="40" width="40" alt="Logo de Novatechnology"> Novatechnology S.A</div>
                        <div class="panel-body">
                            En <a href="http://www.novatechnology.com.ec/" rel="nofollow" target="_blank">Novatechnology</a>, desarrollador web de software CRM a la medida para importantes empresas del Ecuador.
                        </div>
                        <div class="panel-footer">2015 - 2016 y actualmente trabajando como freelancer para la misma empresa</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>