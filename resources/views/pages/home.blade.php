<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Jairo Ushiña, desarrollador web profesional, uso de tecnologías y tendencias modernas, HTML5, CSS3, jQuery, PHP, Laravel, Vue.js, etc." />
        <meta name="author" content="Jairo Ushiña" />
        <!-- Title -->
        <title>Jairo Ushiña | Desarrollador Web</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}">
        <link href='http://fonts.googleapis.com/css?family=Revalia%7COswald%7COpen+Sans+Condensed:300%7CRoboto' rel='stylesheet' type='text/css' />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--[if IE]>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
        <![endif]-->
        @include('pages._analytics')

    </head>
    <body>
        <!-- Hides the complete page until window loads -->
        <div class="cover"></div>
        <!-- container -->
        <div class="container">
            <header>
                @include('pages._navbar')
            </header>
            <!-- Book style flip wrapper -->
            <div class="bb-custom-wrapper">
                <div id="bb-bookblock" class="bb-bookblock">
                    <!-- bb-item represents a page in the resume.
                        add or remove bb-item divs to add or remove pages.
                        -->
                    <!-- First page, your photo and title -->
                    @include('pages._first')
                    <!-- /First page -->
                    <!-- About -->
                    @include('pages._about')
                    <!-- /About -->
                    <!-- Education -->
                    @include('pages._educacion')
                    <!-- /Education -->
                    <!-- Experience -->
                    @include('pages._experiencia')
                    <!-- /Experience -->
                    <!-- Skills -->
                    @include('pages._habilidades')
                    <!-- /Skills -->
                    <!-- Portfolio -->
                    @include('pages._portafolio')
                    <!-- /Portfolio -->
                    <!-- Contact -->
                    @include('pages._contacto')
                    <!-- /Contact -->
                </div>
                <!-- Bottom navigation buttons, hidden in extra small mode -->
                <nav class="hidden-xs">
                    <a href="#" class="bb-nav-prev"><i class="fa fa-angle-left"></i></a>
                    <a href="#" class="bb-nav-next"><i class="fa fa-angle-right"></i></a>
                </nav>
                <!-- /Bottom navigation buttons -->
            </div>
        </div>

        <script src="{{ asset('js/all.js') }}"></script>


        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript" ></script>
        <script src="js/gmaps.js" type="text/javascript"></script>
        <script>
            Page.init();

                function sendContactEmail()
                {

                    $("#ajax-state-contact").show();
                    var params = $("#contact-form").serialize();

                    $.post( "/contacto", params,  function(data) {
                        // alert( "success" );
                        // alert(data);
                        $("#ajax-state-contact").hide();
                        $("#name").val("");
                        $("#email").val("");
                        $("#phone").val("");
                        $("#message").val("");
                    })
                    .done(function() {
                        alert( "El mensaje se envió correctamente." );
                    })
                    .fail(function() {
                        alert( "Hubo un error. Inténtalo más tarde." );
                    })
                    .always(function() {
                        var tracker =_gat._getTracker('UA-79717909-2')
                        tracker._trackEvent('formularios', 'llenar contacto');
                        // alert( "finished" );
                    });
                }

        </script>
        <!-- Theme change plugin, make sure to remove before deploying into production -->
        <!-- <div class="slide-out-div">
            <a class="handle" href="#"></a>
            <h3>Change Color</h3>
            <ul id="color-switcher" class="list-inline">
                <li><a href="#" data-rel="css/theme-colors/orange.css"><img src="images/orange-theme.png" alt="Orange Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/green.css"><img src="images/green-theme.png" alt="Green Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/blue.css"><img src="images/blue-theme.png" alt="Blue Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/red.css"><img src="images/red-theme.png" alt="Red Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/purple.css"><img src="images/purple-theme.png" alt="Purple Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/dark.css"><img src="images/dark-theme.png" alt="Dark Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/glade.css"><img src="images/glade-theme.png" alt="Glade Theme Color" /></a></li>
                <li><a href="#" data-rel="css/theme-colors/kimberly.css"><img src="images/kimberly-theme.png" alt="Kimberly Theme Color" /></a></li>
            </ul>
        </div> -->
    </body>
</html>
