<div class="bb-item" id="page-1">
    <div class="bb-custom-side">
        <div class="bb-custom-container">
            <h1 class="text-center"><span class="page-heading">Jairo</span></h1>
            <div class="row content-block text-justify animateFadeInRight">
                <div class="col-sm-2 col-sm-offset-2 text-center">
                    <img src="images/man.png" alt="man icon"/>
                </div>
                <div class="col-sm-6">
                    <p>Bienvenidos! Soy Jairo, nací en la ciudad de Ibarra y actualmente vivo en Durán, provincia del Guayas. En este espacio podrás conocer un poco más de mi y de mi trabajo, si te interesa puedes echar un vistazo.  
                    </p>
                </div>
            </div>
            <div class="row content-block text-justify animateFadeInLeft">
                <div class="col-sm-2 col-sm-offset-2 col-sm-push-6 text-center">
                    <img src="images/joypad.png" alt="joypad icon"/>
                </div>
                <div class="col-sm-6 col-sm-pull-2">
                    <p>
                        Desde niño tuve inclinación hacia los computadores, video juegos y tecnología. De hecho, me gustan muchos lo video juegos, creo que todo diseñador, programador, desarrollador, etc., debería tener en su historial video juegos que haya, valga la redundancia, jugado y terminado. Después de todo, los video juegos nos enseñaron cuando niños sobre, tecnología, que es un error de programación, que son variables, que es un servidor. Quienes jugamos Prince of Persia, en un 486 con DOS, incluso tuvimos que aprender a manejar consola y comandos del sistema operativo.
                    </p>
                </div>
            </div>
            <div class="row content-block content-block-last text-justify animateFadeInRight">
                <div class="col-sm-2 col-sm-offset-2 text-center">
                    <img src="images/code.png" alt="wine icon"/>
                </div>
                <div class="col-sm-6">
                    <p>
                        Actualmente soy programador, me dedico al desarrollo web. Disfruto mucho el que la gente conozca y utilice las cosas que desarrollo. Esa es la razón por la que me he dedicado tanto al mundo del desarrollo para internet, pues lo que se publica ahí lo puedo compartir con gente de todo el mundo.
                    </p>
                    <p>
                        Mis trabajos los desarrollo usando HTML5, CSS3, JAVASCRIPT, jQuery, Vue, etc., para el front-end. En la parte del backend, la parte en la que me he especializo más a fondo, uso actualmente PHP con Laravel como framework. La filosofía de "beautiful code" que persigue Laravel y su comunidad, es algo con lo que concuerdo totalmente y que marca el trabajo que realizo.
                    </p>

                    <p>¡Bienvenidos!<br><br></p>
                </div>
            </div>
        </div>
    </div>
</div>