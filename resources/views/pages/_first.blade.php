<div class="bb-item" id="page-0">
    <div id="bb-first-page" class="bb-custom-side">
        <div class="row" id="first-page">
            <div class="col-sm-12 text-center">
                <div class="dp-box">
                    <h1 class="hidden-sm hidden-xs"><img class="dp" id="dp" src="images/jairo_ushina.png" alt="Jairo Ushiña foto" /> Jairo Ushiña</h1>
                    <div class="visible-xs visible-sm">
                        <img class="dp" id="dp" src="images/jairo_ushina.png" alt="Jairo Ushiña foto" />
                        <h1>Jairo Ushiña</h1>
                    </div>
                </div>
            </div>
            <div class="col-xs-8 col-xs-offset-2 text-center">
                <h2>
                    DESARROLLADOR WEB
                </h2>
                <p class="social-buttons">
                    <a class="btn btn-circle" href="http://google.com/+JairoUshi%C3%B1a" target="_blank"><i class="fa fa-google"></i></a>
                    <a class="btn btn-circle" href="https://twitter.com/rjairo" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a class="btn btn-circle" href="#" data-page="7" onclick="$('#contact-page').trigger('click'); return false;" ><i class="fa fa-envelope"></i></a>
                    <a class="btn btn-circle" href="https://github.com/jago86/" target="_blank"><i class="fa fa-github"></i></a>
                    <a class="btn btn-circle" href="http://blog.jairo.rocks" target="_blank" title="Blog"><i class="fa fa-file-text"></i></a>
                </p>
                <p id="navigation-help">
                    Para navegar utiliza las teclas <i class="fa fa-arrow-left"></i> <i class="fa fa-arrow-right"></i>
                </p>
            </div>
        </div>
    </div>
</div>