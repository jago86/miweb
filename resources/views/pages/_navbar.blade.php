<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-top">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- Top navigation buttons, visible only in extra small mode -->
            <a class="visible-xs bb-nav-prev navbar-brand navbar-brand-pagination" href="#"><i class="fa fa-angle-left"></i></a>
            <a class="visible-xs bb-nav-next navbar-brand navbar-brand-pagination" href="#"><i class="fa fa-angle-right"></i></a>
            <!-- /Top navigation buttons -->
        </div>
        <div id="nav-top" class="collapse navbar-collapse pageScrollerNav standardNav dark">
            <ul class="nav navbar-nav navbar-right" id="menu">
                <li><a href="#"  data-page="1">Inicio</a></li>
                <li><a href="#" data-page="2">Jairo</a></li>
                <li><a href="#" data-page="3">Estudios</a></li>
                <li><a href="#" data-page="4">Experiencia</a></li>
                <li><a href="#" data-page="5">Habilidades</a></li>
                <li><a href="#" data-page="6">Trabajo</a></li>
                <li><a id="contact-page" href="#" data-page="7">Contacto</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>