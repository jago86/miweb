<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::post('/contacto', function (Request $request) {
	Mail::send('emails.contacto', compact('request'), function ($m) {
		$m->from('jago86@gmail.com', 'Sitio web JAIRO.ROCKS');
        $m->to('jago86@gmail.com', 'Jairo Ushiña')->subject('Jairo.rocks, formulario de contacto');
    });
});

// Route::auth();

// Route::get('/home', 'HomeController@index');
