var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/css/_sass.css');

    mix.styles([
    	// 'bootstrap.css',
    	'font-awesome.css',
    	'animate.css',
		'simpletextrotator',
		'theme-loading-bar.css',
		'theme.css',
		'bookblock.css',
		'css-loader.css',
		'theme-colors/kimberly.css',
		'custom-css.css',
    ], 'public/css/_css.css');

    mix.styles([
    	'./public/css/_sass.css',
    	'./public/css/_css.css',
    ]);

    /**
     * SCRIPTS
     */
    mix.scripts([
    	'modernizr.custom.js',
		'pace.js',
		'jquery.min.js',
		'./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
		'theme.js',
		'jquery.bookblock.js',
		'bootstrap-progressbar.min.js',
		'jquery.tabSlideOut.v1.3.js',
		// 'gmaps.js',
    ]);

    mix.copy('resources/assets/js/gmaps.js', 'js/gmaps.js');
});
